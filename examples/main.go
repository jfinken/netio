package main

import (
	"log"
	"os"
	"os/signal"
	"reflect"
	"syscall"

	"bitbucket.org/jfinken/netio/node"
)

func parseLoc(data node.EpMessage) {
	if loc, ok := data.(node.SensorLocation); ok {
		log.Println(loc)
	} else {
	}
}
func main() {
	_ = "A Go port of netio\n"

	/*
		TODO:
		- unit test for broadcast address discovery
		- proper shutdown of goroutines and sockets

		DONE:
		- generate a (truncated) UUID identity.
		- auto-discover the broadcast address
		- broadcast and receive beacons over UDP:9999, ignoring self.
		- generate emphemeral port for TCP.
		- Fill out EpNode and Peer structs.
		- bind and connect ROUTER-DEALER sockets.
		- send and receive Capability structs
		- peer management and reaping
		- Initial zmq PUB-SUB sockets
	*/
	c1 := node.EpNodeCapability{
		Action: node.Subscriber,
		Name:   "Nigel Tufnel",
		// SubTopic: []node.Str{node.Raw},
		SubTopic: []node.Str{"vps-pose"},
	}
	n1 := node.NewEpNode(&c1)

	// End-client: retrieve the topical channel receive objects of each type of interest
	// raw, ok := n1.GetSubtopicChannel(node.Raw)
	raw, ok := n1.GetSubtopicChannel("vps-pose")
	n1.Start()

	if ok {
		go func() {
			for {
				loc := <-raw
				log.Println(reflect.TypeOf(loc))
			}
		}()
	}

	/*
		c2 := node.EpNodeCapability{
			Action:   node.Publisher,
			Name:     "Derek Smalls",
			PubTopic: []node.Str{node.Raw},
		}
		n2 := node.EpNode{}
		n2.Start(&c2)
	*/

	// block until SIGTERM
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	log.Printf("Ctrl-C to quit...\n")
	<-c
	os.Exit(0)
}
