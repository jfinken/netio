netio
---

#### Overview

Proximity-based, peer-to-peer, real-time messaging exchange in Go.

#### Installation

* msgpack for Go: tinylib
* `SO_REUSEADDR` support

    aptitude install libzmq3-dev

    go get github.com/pebbe/zmq4
    go get github.com/libp2p/go-reuseport
    go get -u -t github.com/tinylib/msgp

    make

This is a WIP.
