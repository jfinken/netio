
all:
	cd node; go generate; cd ..;
	go build
	go test -v ./...
	go install

ex:
	cd examples; go build -o netio; mv netio ..
	cd ..
