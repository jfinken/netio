package node

import (
	"crypto/rand"
	"fmt"
	"io"
	"log"

	zmq "github.com/pebbe/zmq4"
)

const (
	broadcastPort = "9999"
)

// EpNode defines our top-level node structure
type EpNode struct {
	Name       string
	peerSvc    *peerSvc
	subs       map[string]chan EpMessage
	Capability *EpNodeCapability
}

// NewEpNode constructs and prepares a new node before use (before Start)
func NewEpNode(c *EpNodeCapability) *EpNode {
	// Subscriptions
	n := EpNode{}
	// Required, but obviate the need for the client to assign.
	// Also do not want to use reflection as it includes the package.
	n.Capability = c
	n.Capability.Type = "EpNodeCapability"
	peerSvc, err := newPeerSvc()
	if err != nil {
		log.Fatal(err)
	}
	n.peerSvc = peerSvc
	// Generate a UUID for our node, first N chars
	uuid, err := newUUID()
	if err != nil {
		log.Fatal(err)
	}
	uuid = uuid[0:10]
	n.Capability.UUID = Str(uuid)

	// If this capability specifies any subtopics, make corresponding
	// reception channels here
	if len(n.Capability.SubTopic) > 0 {
		n.subs = make(map[string]chan EpMessage)
		for _, topic := range n.Capability.SubTopic {
			n.subs[string(topic)] = make(chan EpMessage)
		}
	}
	return &n
}

// Start broadcasts the beacon over a goroutine and listens for incoming beacons
func (n *EpNode) Start() {

	if n.Capability == nil {
		log.Fatal(fmt.Errorf("EpNodeCapability is nil for EpNode %v", n))
	}
	// TODO: sigterm chan?
	// chan to receive a common topic of interest and EpNodeCapability messages from peers
	c := make(chan Publish)
	n.peerSvc.Start(n.Capability, c)

	go n.handlePublisher(c)
}

// GetSubtopicChannel is a convenience function returning the chan corresponding to topic
func (n *EpNode) GetSubtopicChannel(topic string) (chan EpMessage, bool) {
	ch, ok := n.subs[topic]
	return ch, ok
}

func (n *EpNode) handlePublisher(c chan Publish) {
	for {
		pub := <-c
		// TODO:
		//	- deal with proper goroutine and socket shutdown
		//	- this is a goroutine that creates a goroutine?
		go n.subscribe(pub)
	}
}
func (n *EpNode) subscribe(pub Publish) {
	//  Prepare our sub
	sub, _ := zmq.NewSocket(zmq.SUB)
	defer sub.Close()
	sub.Connect(fmt.Sprintf("tcp://%s:%d", pub.Conn.PeerIPAddr, pub.Conn.PubPort))
	sub.SetSubscribe(string(pub.Topic))

	for {
		//  Read envelope with topic
		topic, _ := sub.Recv(0)
		//  Read message contents (blocking until a message has arrived)
		data, _ := sub.RecvBytes(0)
		// Dispatch to LUT for unmarshaling
		err := n.dispatch(topic, data)
		if err != nil {
			log.Println(err)
		}
	}
}

// Dispatch FIXME: better way to avoid conditional logic (e.g. LUT)?
func (n *EpNode) dispatch(topic string, data []byte) error {
	// 1. Decode to the "generic" type
	// 2. Examine then unmarshal to the specific type
	var msg EpBaseMessage
	_, err := msg.UnmarshalMsg(data)
	if err != nil {
		return err
	}
	// HOWTO:
	//	- move the concrete data types *outside* of the netio library?
	//	- and/or avoid conditional logic with UnmarshalMsg, reflection?
	if ch, ok := n.subs[topic]; ok {
		switch msg.Type() {
		case "SensorLocation":
			var loc SensorLocation
			_, err := loc.UnmarshalMsg(data)
			if err != nil {
				return err
			}
			ch <- loc
			return nil
		case "SensorCatalystLocation":
			var loc SensorCatalystLocation
			_, err := loc.UnmarshalMsg(data)
			if err != nil {
				return err
			}
			ch <- loc
			return nil
		case "VpsPose":
			var loc VpsPose
			_, err := loc.UnmarshalMsg(data)
			if err != nil {
				return err
			}
			ch <- loc
			return nil
		}
	}
	return fmt.Errorf("Dispatch unknown type: %s", msg.Type())
}

// newUUID generates a random UUID according to RFC 4122.  Note 0x00 is
// reserved for zmq infrastructure, append a "1".
func newUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6]&^0xf0 | 0x40
	// "%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]
	return fmt.Sprintf("1%x%x%x%x%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
