package node

import (
	"log"
	"net"

	reuse "github.com/libp2p/go-reuseport"
)

const (
	maxDatagramSize = 8192
)

// Listen binds to the UDP address and port given and writes packets received
// from that address to a buffer which is passed to a hander
//func Listen(address string, handler func(*net.UDPAddr, int, []byte)) {
func Listen(address string, handler func(net.Addr, int, []byte)) {
	// Parse the string address
	//addr, err := net.ResolveUDPAddr("udp", address)
	_, err := reuse.ResolveAddr("udp", address)
	if err != nil {
		log.Fatal(err)
	}

	// Open up a connection
	// conn, err := net.ListenUDP("udp", addr)
	conn, err := reuse.ListenPacket("udp", address)
	if err != nil {
		log.Fatal(err)
	}

	//conn.SetReadBuffer(maxDatagramSize)

	// Loop forever reading from the socket
	for {
		buffer := make([]byte, maxDatagramSize)
		numBytes, src, err := conn.ReadFrom(buffer)
		if err != nil {
			log.Fatal("ReadFromUDP failed:", err)
		}

		handler(src, numBytes, buffer)
	}
}
