//go:generate msgp
//msgp:ignore PeerIPAddr

package node

import (
	"net"
)

// EpNodeBeacon holds data intended for Node broadcast beacons.
type EpNodeBeacon struct {
	UUID       string `msg:"uuid"`
	InboxPort  int    `msg:"inboxPort"`
	PeerIPAddr string `msg:"peerIpAddr"`
	Type       string `msg:"type"`
}

// NewBroadcaster creates a new UDP multicast connection on which to broadcast
func NewBroadcaster(address string) (*net.UDPConn, error) {
	addr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return nil, err
	}

	conn, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
