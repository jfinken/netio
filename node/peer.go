package node

import (
	"fmt"
	"log"
	"time"

	zmq "github.com/pebbe/zmq4"
)

// Peer defines the data for peer management
type Peer struct {
	UUID      string
	IPAddr    Str
	Expiry    int
	ExpiresAt int
	Mailbox   *zmq.Socket
}

// NewPeer constructs and return a pointer to a Peer
func NewPeer(uuid string) *Peer {
	p := new(Peer)
	p.UUID = uuid
	p.Expiry = 45 // sec
	p.setAlive()

	return p
}
func (p *Peer) connect(identity, peerIP string, port int) error {
	socket, err := zmq.NewSocket(zmq.DEALER)
	if err != nil {
		return err
	}
	p.Mailbox = socket
	p.Mailbox.SetIdentity(identity)
	p.Mailbox.SetSndtimeo(0)
	p.IPAddr = Str(peerIP)

	endpoint := fmt.Sprintf("tcp://%s:%d", peerIP, port)
	log.Printf("Peered to %s at %s\n", p.UUID, endpoint)
	err = p.Mailbox.Connect(endpoint)

	return err
}
func (p *Peer) send(capability *EpNodeCapability) error {
	bts, err := capability.MarshalMsg(nil)
	if err != nil {
		log.Fatal(err) // print err or log error information
	}
	_, err = p.Mailbox.SendBytes(bts, zmq.DONTWAIT)
	return err
}
func (p *Peer) setAlive() {
	p.ExpiresAt = int(time.Now().Unix()) + p.Expiry
}

// disconnect will close this peer's DEALER socket.
func (p *Peer) disconnect() {
	p.Mailbox.Close()
}
