//go:generate msgp
//msgp:shim Str as:[]byte using:tobin/frombin

package node

import (
	"reflect"
)

// Action simply enumerates a Node's action as a subscriber, publisher or both
type Action int

// Str and the above msgp shims exists to properly decode incoming byte-strings
// E.g. https://github.com/tinylib/msgp/issues/31
// (I'm looking at you python)
type Str string

// Action simply enumerates a Node's action as a subscriber, publisher or both
const (
	Publisher Action = iota
	Subscriber
	SubscriberPublisher
)

// Publish defines the necessary details for a subscriber to connect over
// PUB-SUB
type Publish struct {
	Topic Str
	Conn  EpNodeCapability
}

// RAW et al describe topics.  Users are free to define their own.
const (
	Raw    = "sensor@frame"
	RawImg = "rawimg@frame"
	RawTel = "telem@frame"
	Pose   = "pose@frame"
)

func tobin(s Str) []byte   { return []byte(s) }
func frombin(b []byte) Str { return Str(string(b)) }

// EpNodeCapability holds data intended for semantic node discovery.
type EpNodeCapability struct {
	Action     Action `msg:"action"`
	UUID       Str    `msg:"uuid"`
	Name       Str    `msg:"name"`
	SubTopic   []Str  `msg:"subTopic"`
	PubTopic   []Str  `msg:"pubTopic"`
	PubPort    int    `msg:"pubPort"`
	PeerIPAddr Str    `msg:"peerIpAddr"`
	IPAddr     Str    `msg:"ipAddr"`
	InboxPort  int    `msg:"inboxPort"`
	Type       Str    `msg:"type"`
	Indexing   bool   `msg:"indexing"`
	HTTPPort   int    `msg:"httpPort"`
}

// EpMessage defines the RAW message interface
type EpMessage interface {
	Type() string
}

// EpBaseMessage et al define the RAW message types
type EpBaseMessage struct {
	Typ string `msg:"type"`
}

// Type returns the string representation of the concrete type
func (m EpBaseMessage) Type() string {
	return m.Typ
}

// SensorLocation defines a general location type
type SensorLocation struct {
	Latitude           float64 `msg:"latitude"`
	Longitude          float64 `msg:"longitude"`
	Altitude           float64 `msg:"altitude"`
	Speed              float64 `msg:"speed"`
	Bearing            float64 `msg:"bearing"`
	Accuracy           float64 `msg:"accuracy"`
	MonotonicTimestamp int     `msg:"monotonicTimestamp"`
	Timestamp          int     `msg:"timestamp"`
}

// Type implements the EpNodeMessage interface
func (l SensorLocation) Type() string {
	return reflect.TypeOf(l).String()
}

// Type implements the EpNodeMessage interface
func (l SensorCatalystLocation) Type() string {
	return reflect.TypeOf(l).String()
}

// SensorCatalystLocation defines a struct intending to hold data from
// high-definition data sources
type SensorCatalystLocation struct {
	Latitude           float64 `msg:"latitude"`
	Longitude          float64 `msg:"longitude"`
	Height             float64 `msg:"height"`
	Heading            float64 `msg:"heading"`
	CorrectionAge      float64 `msg:"-"`
	Pdop               float64 `msg:"pdop"`
	Hdop               float64 `msg:"hdop"`
	Vdop               float64 `msg:"vdop"`
	GpsTime            int     `msg:"gpsTime"`
	Solution           Str     `msg:"solution"`
	ReferenceFrame     Str     `msg:"-"`
	NumberSatellites   int     `msg:"numberSatellites"`
	StationID          int     `msg:"-"`
	HorizontalVelocity float64 `msg:"horizontalVelocity"`
	VerticalVelocity   float64 `msg:"VerticalVelocity"`
	HPrecision         float64 `msg:"hprecision"`
	VPrecision         float64 `msg:"vprecision"`
	StaticEpochs       int     `msg:"-"`
	Epoch              float64 `msg:"-"`
	MonotonicTimestamp int     `msg:"monotonicTimestamp"`
	Timestamp          int     `msg:"timestamp"`
}

// VpsPose defines the structure of the VPS response. It should
// absolutely go somewhere else.
type VpsPose struct {
	LatitudeIn       float64 `msg:"latitude_in"`
	LatitudeOut      float64 `msg:"latitude_out"`
	LongitudeIn      float64 `msg:"longitude_in"`
	LongitudeOut     float64 `msg:"longitude_out"`
	AltitudeIn       float64 `msg:"altitude_in"`
	AltitudeOut      float64 `msg:"altitude_out"`
	HeadingIn        float64 `msg:"heading_in"`
	HeadingOut       float64 `msg:"heading_out"`
	Ninlier          int     `msg:"ninlier"`
	PositionDiff     float64 `msg:"position_diff"`
	FaissQuery       float64 `msg:"faiss_qry"`
	FeatureExt       float64 `msg:"feat_ext"`
	PayloadTime      float64 `msg:"payload_transfer_time"`
	PnP              float64 `msg:"pnp"`
	PointCloudLoaded float64 `msg:"point_cloud_loaded"`
	SuppMch          float64 `msg:"supp_mch"`
	TotalTime        float64 `msg:"total_time"`
	VpsTime          float64 `msg:"vps_time"`
	/*
		    rotation_out_x: float
		    rotation_out_y: float
			rotation_out_z: float
	*/
}

// Type implements the EpNodeMessage interface
func (l VpsPose) Type() string {
	return reflect.TypeOf(l).String()
}
