package node

import (
	"encoding/binary"
	"errors"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	zmq "github.com/pebbe/zmq4"
)

// peerSvc controls the UDP broadcast and peering
type peerSvc struct {
	Capability *EpNodeCapability
	Inbox      *zmq.Socket
	InboxPort  int
	Peers      map[string]*Peer
}

// newPeerSvc constructs a new peerSvc, creates and binds the ROUTER socket
func newPeerSvc() (*peerSvc, error) {

	p := peerSvc{Peers: make(map[string]*Peer)}
	socket, err := zmq.NewSocket(zmq.ROUTER)
	if err != nil {
		return nil, err
	}
	p.Inbox = socket
	// Bind to a random port
	port, err := getFreePort()
	if err != nil {
		log.Fatal(err)
	}
	p.InboxPort = port
	// log.Printf("newPeerSvc p.InboxPort: %d\n", p.InboxPort)
	err = p.Inbox.Bind(fmt.Sprintf("tcp://*:%d", p.InboxPort))

	return &p, nil
}

func (p *peerSvc) broadcast(addr string, uuid Str) {
	conn, err := NewBroadcaster(addr)
	if err != nil {
		log.Fatal(err)
	}

	beacon := EpNodeBeacon{UUID: string(uuid), InboxPort: p.InboxPort, Type: "EpNodeBeacon"}
	bts, err := beacon.MarshalMsg(nil)
	if err != nil {
		log.Fatal(err) // print err or log error information
	}
	for {
		// Send our beacon
		conn.Write(bts)

		// remove any dead peers
		p.reapPeers()

		time.Sleep(1 * time.Second)
	}
}

// Start is the main entry point to the discovery and peering process
func (p *peerSvc) Start(capability *EpNodeCapability, c chan Publish) {

	// Get the broadcast address, and get the IP address and assign
	// it to my capability
	p.Capability = capability
	p.Capability.InboxPort = p.InboxPort

	broadcastAddress, ipAddr, err := getBroadcastAndAddress()
	if err != nil {
		log.Fatal(err)
	}
	p.Capability.IPAddr = Str(ipAddr.String())
	fullBroadcastAddr := fmt.Sprintf("%s:%s", broadcastAddress, broadcastPort)

	log.Printf("[IPAddr=%s, uuid=%v] Broadcasting over: %v\n",
		p.Capability.IPAddr, p.Capability.UUID, fullBroadcastAddr)

	// TODO: pass in the SIGTERM chan?
	go p.recvInbox(c)
	go p.broadcast(fullBroadcastAddr, p.Capability.UUID)
	go Listen(fullBroadcastAddr, p.recvBeacon)
}

// recvBeacon receives incoming beacons (including my own)
func (p *peerSvc) recvBeacon(src net.Addr, numBytes int, b []byte) {
	// log.Println(hex.Dump(b[:30]))
	var beacon EpNodeBeacon
	_, err := beacon.UnmarshalMsg(b)
	if err != nil {
		log.Printf("UnmarshalMsg err: %s\n", err.Error())
	}
	// Ignore self
	if beacon.UUID == string(p.Capability.UUID) {
		return
	}

	// log.Printf("%d bytes from %s, other UUID: %v\n", numBytes, src, beacon.UUID)

	beacon.PeerIPAddr = strings.Split(src.String(), ":")[0]

	// New peer
	if p.Peers[beacon.UUID] == nil {
		p.recvPeer(beacon.UUID, beacon.PeerIPAddr, beacon.InboxPort)
	} else {
		p.Peers[beacon.UUID].setAlive()
	}
}
func (p *peerSvc) recvPeer(peerUUID, peerIPAddr string, peerInboxPort int) {

	peer := NewPeer(peerUUID)
	// ROUTER-DEALER connect
	err := peer.connect(string(p.Capability.UUID), peerIPAddr, peerInboxPort)
	if err != nil {
		log.Printf("peer.connect err: %s\n", err.Error())
	}
	// Store peer in map
	p.Peers[peerUUID] = peer

	// Send peer my capability
	err = peer.send(p.Capability)
	if err != nil {
		log.Printf("peer.send err: %s\n", err.Error())
	}
}

// recvInbox is intended to be a goroutine to service the inbox socket and
// receive capability messages from peers.
func (p *peerSvc) recvInbox(c chan Publish) {

	// Initialize and poll
	poller := zmq.NewPoller()
	poller.Add(p.Inbox, zmq.POLLIN)

	for {
		sockets, _ := poller.Poll(1 * time.Second)
		for _, socket := range sockets {
			switch s := socket.Socket; s {
			case p.Inbox:
				var capability EpNodeCapability
				data, err := s.RecvMessageBytes(zmq.DONTWAIT)
				if err != nil {
					log.Printf("RecvMessageBytes err: %s\n", err.Error())
				}
				// Framed, so data[0] is the sender's identity
				// Deserialize EpNodeCapability
				_, err = capability.UnmarshalMsg(data[1])
				if err != nil {
					log.Printf("UnmarshalMsg err: %s\n", err.Error())
				}
				// Peer discovery is not neatly synchronized.  Here we've received the
				// capability message from the peer *before* its UDP beacon.  Thus,
				// utilize recvPeer to formally finish the peering.
				_, ok := p.Peers[string(capability.UUID)]
				if !ok {
					p.recvPeer(string(capability.UUID), string(capability.IPAddr), capability.InboxPort)
				}
				capability.PeerIPAddr = p.Peers[string(capability.UUID)].IPAddr

				pub, topic, capability := p.isPublisherOfInterest(capability)
				// Send upstream the common topic and received peer's capability as a tuple
				if pub {
					c <- Publish{Topic: topic, Conn: capability}
				}
			}
		}
	}
}

// reapPeers will delete any peer from whom no beacon has been received in expiry seconds.
func (p *peerSvc) reapPeers() {
	now := int(time.Now().Unix())
	for key, peer := range p.Peers {
		if peer.ExpiresAt < now {
			log.Printf("Reaping: %s\n", peer.UUID)
			peer.disconnect()

			delete(p.Peers, key)
		}
	}
}

// Whether or not the peer is a publisher to a topic of interest, return also this topic
// and the capability itself
func (p *peerSvc) isPublisherOfInterest(c EpNodeCapability) (bool, Str, EpNodeCapability) {
	if c.Action == Subscriber {
		return false, "", c
	}
	inter := intersection(c.PubTopic, p.Capability.SubTopic)
	if len(inter) > 0 {
		return true, inter[0], c
	}
	return false, "", c
}

// Return the intersection of s1 and s2
func intersection(s1, s2 []Str) (inter []Str) {
	hash := make(map[Str]bool)
	for _, e := range s1 {
		hash[e] = true
	}
	for _, e := range s2 {
		// If elements present in the hashmap then append intersection list.
		if hash[e] {
			inter = append(inter, e)
		}
	}
	// Remove duplicates
	inter = removeDups(inter)
	return
}

// Remove dups from the slice
func removeDups(elements []Str) (nodups []Str) {
	encountered := make(map[Str]bool)
	for _, element := range elements {
		if !encountered[element] {
			nodups = append(nodups, element)
			encountered[element] = true
		}
	}
	return
}
func contains(s []Str, e Str) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// getBroadcastAndAddress retrieves the broadcast addr of the local network. Note it will currently
// return the first valid address found.
func getBroadcastAndAddress() (string, net.IP, error) {

	var broadcastAddress string
	var ip net.IP

	ifaces, err := net.Interfaces()
	if err != nil {
		return "", nil, err
	}
	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			return "", nil, err
		}
		// ignore loopback
		if iface.Flags != net.FlagUp|net.FlagBroadcast|net.FlagMulticast {
			continue
		}

		for _, addr := range addrs {
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
				broadcast, err := lastAddr(v)
				if err != nil {
					log.Printf("Warning %s: %v\n", err.Error(), ip)
					continue
				}
				if broadcast != nil {
					broadcastAddress = broadcast.String()
					return broadcastAddress, ip, nil
				}
			case *net.IPAddr:
				//ip = v.IP
			}
		}
	}
	return broadcastAddress, ip, nil
}

// lastAddr returns the broadcast IP address given the the IPNet
// Citing:
// https://stackoverflow.com/questions/36166791/how-to-get-broadcast-address-of-ipv4-net-ipnet/36167611
func lastAddr(n *net.IPNet) (net.IP, error) { // works when the n is a prefix, otherwise...
	if n.IP.To4() == nil {
		return net.IP{}, errors.New("Does not support IPv6 addresses")
	}
	ip := make(net.IP, len(n.IP.To4()))
	binary.BigEndian.PutUint32(ip, binary.BigEndian.Uint32(n.IP.To4())|^binary.BigEndian.Uint32(net.IP(n.Mask).To4()))
	return ip, nil
}

// getFreePort asks the kernel for a free open port that is ready to use.
func getFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}
